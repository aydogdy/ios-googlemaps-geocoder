//
//  UITextField.swift
//  LatLngAddress
//
//  Created by Aydogdy Shahyrov on 02/12/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

extension UITextField {
    
    func textFieldStyle(pHolder: String) {
        self.placeholder = pHolder
        self.font = UIFont.systemFont(ofSize: 15)
        self.borderStyle = UITextField.BorderStyle.roundedRect
        self.keyboardType = UIKeyboardType.numbersAndPunctuation
        self.returnKeyType = UIReturnKeyType.done
        self.clearButtonMode = UITextField.ViewMode.whileEditing;
        self.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        self.delegate = self as? UITextFieldDelegate
    }
    
}

