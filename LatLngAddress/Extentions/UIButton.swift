//
//  UIButton.swift
//  LatLngAddress
//
//  Created by Aydogdy Shahyrov on 02/12/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

extension UIButton {
    
    func buttonStyle(text: String, color: UIColor) {
        self.backgroundColor = color
        self.setTitle(text, for: .normal)
    }
    
}
