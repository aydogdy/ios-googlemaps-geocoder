//
//  ViewController.swift
//  LatLngAddress
//
//  Created by Aydogdy Shahyrov on 01/12/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.

// Google maps API key: AIzaSyA2uXKml5y98uMBfBxVC7ZTd4uI8FmIqc4

import UIKit
import GoogleMaps

class ViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    
    //MARK: Declaratin instance variables
    let apiKey = "AIzaSyA2uXKml5y98uMBfBxVC7ZTd4uI8FmIqc4"
    var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    var defaultLocation = CLLocationCoordinate2D(latitude: 42.360091, longitude: -71.09415999999999)
    var initLocation = CLLocationCoordinate2D(latitude: 42.360091, longitude: -71.09415999999999)
    let zoomValue = Float(16.00)
    
    let defaultLocationBtn = UIButton(frame: CGRect(x: 20, y: 170, width: 115, height: 40))
    let userLocationBtn = UIButton(frame: CGRect(x: 140, y: 170, width: UIScreen.main.bounds.size.width-160, height: 40))
   
    let latTextField:UITextField =  UITextField(frame: CGRect(x: 20, y: 80, width: UIScreen.main.bounds.size.width-40, height: 40))
    let lngTextField:UITextField =  UITextField(frame: CGRect(x: 20, y: 125, width: UIScreen.main.bounds.size.width-40, height: 40))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTheLocationManager();
        initMapView()
    }
    
    
    //MARK: Setup the location manager
    func initTheLocationManager() {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
    }
    
    //MARK: Init Google map
    func initMapView() {
        GMSServices.provideAPIKey(apiKey)
        
        let camera = GMSCameraPosition.camera(withLatitude: initLocation.latitude, longitude: initLocation.longitude, zoom: zoomValue)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        
        displayAddress(lat: defaultLocation.latitude, lng: defaultLocation.longitude)
        
        mapView.settings.consumesGesturesInView = false;
        initUI()
    }
    
    func initUI() {
        latTextField.textFieldStyle(pHolder: "Enter latitude")
        
        lngTextField.textFieldStyle(pHolder: "Enter longitude")
        
        defaultLocationBtn.buttonStyle(text: "Default", color: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1))
        defaultLocationBtn.addTarget(self, action: #selector(moveToDefaulLocation), for: .touchUpInside)
        
        userLocationBtn.buttonStyle(text: "Find address", color: #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1))
        userLocationBtn.addTarget(self, action: #selector(moveToUserLocation), for: .touchUpInside)
        
        self.view.addSubview(latTextField)
        self.view.addSubview(lngTextField)
        self.view.addSubview(userLocationBtn)
        self.view.addSubview(defaultLocationBtn)
    }

    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
    @objc func moveToDefaulLocation(sender: UIButton!){
        latTextField.text = ""
        lngTextField.text = ""
        
        view.endEditing(true)
        
        CATransaction.begin()
            CATransaction.setValue(1, forKey: kCATransactionAnimationDuration)
            mapView.animate(to: GMSCameraPosition.camera(withTarget: defaultLocation, zoom: zoomValue))
        CATransaction.commit()
        
        displayAddress(lat: defaultLocation.latitude, lng: defaultLocation.longitude)
    }
    
    @objc func moveToUserLocation(sender: UIButton!) {
        
        guard let lat = latTextField.text, !lat.isEmpty else {
            return
        }
        guard let lng = lngTextField.text, !lng.isEmpty else {
            return
        }
        initLocation.latitude = Double(lat) ?? 0
        initLocation.longitude = Double(lng) ?? 0
        
        view.endEditing(true)
        
        CATransaction.begin()
            CATransaction.setValue(1, forKey: kCATransactionAnimationDuration)
            mapView.animate(to: GMSCameraPosition.camera(withTarget: initLocation, zoom: zoomValue))
        CATransaction.commit()
        
        displayAddress(lat: initLocation.latitude,  lng: initLocation.longitude)
    }
    
    
    func displayAddress(lat: Double, lng: Double){
        
        geocode(latitude: lat, longitude: lng) { placemark, error in
            guard let placemark = placemark, error == nil else { return }
            
            DispatchQueue.main.async {
                
                let currentLocation = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                let marker = GMSMarker(position: currentLocation)
                
                let title = "\(placemark.subThoroughfare  ?? "") \(placemark.thoroughfare ?? "")"
                let snippest = "\(placemark.locality  ?? "") \(placemark.administrativeArea  ?? "") \(placemark.postalCode  ?? "") \( placemark.country  ?? "Oops! Address not available!")"
                
                marker.title = title
                marker.snippet = snippest
                marker.map = self.mapView
                self.mapView.selectedMarker = marker
            }
        }
    }
   
}

